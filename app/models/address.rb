class Address < ApplicationRecord
  # Tem o id do contact aqui
  # Como usamos o address_attributes, passar optional true para permitir cadastrar um endereço antes
  belongs_to :contact, optional: true
end
