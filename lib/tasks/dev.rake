namespace :dev do
  desc "Configura o ambiente de desenvolvimento"
  task setup: :environment do

    puts "Resetando o banco de dados..."
    %x(rails db:drop db:create db:migrate)

    puts "Criando tipos de contatos..."

    kinds = %w(Amigo Comercial Conhecido)

    kinds.each do |kind| 
      Kind.create!(
        description: kind
      )
    end

    puts "Terminou de criar tipos de contatos..."

    ####################################

    puts "Criando contatos..."

    100.times do |i|
      Contact.create!(
        name: Faker::Name.name,
        email: Faker::Internet.email,
        birthdate: Faker::Date.between(from: 65.years.ago, to: 18.years.ago), 
        kind: Kind.all.sample
      )  
    end

    puts "Terminou de criar contatos..."

    ###################################

    puts "Criando telefones..."

    Contact.all.each do |contact|
      Random.rand(5).times do |i|
        phone = Phone.create!(number: Faker::PhoneNumber.cell_phone, contact: contact)
        contact.phones << phone
      end
      contact.save!
    end

    puts "Terminou de criar telefones..."

        ###################################

    puts "Criando endereços..."

    Contact.all.each do |contact|
      Address.create!(
        street: Faker::Address.street_name,
        city: Faker::Address.city,
        contact: contact
      )
    end

    puts "Terminou de criar endereços..."

  end
end
